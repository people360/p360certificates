import Moment from "react-moment"

export default function DataCertificate(props) {

    const certificate = props.data
    const certificate_number = certificate.certificate_number_field_1 +'-'+ certificate.certificate_number_field_2;
    const pdf = ( certificate.file != null ) ? certificate.file.url + '#zoom=80' : process.env.STRAPI_API_AUTOMATION_URL + 'files/view/' + certificate.file_default + '#zoom=80';

    return (
        <div className="Container">
            <div className="Certificate">
                <div className="Banner">
                    <h1>Certification</h1>
                    <h2>Registry</h2>
                </div>
                
                <div className="Grid">
                    <div className="Grid-Item-1">
                        <div className="Info">
                            <div className="Info-Item">
                                <div className="Label">Document Name:</div>
                                <div className="Description">{certificate.document_name}</div>
                            </div>
                            <div className="Info-Item">
                                <div className="Label">Company:</div>
                                <div className="Description">{certificate.company}</div>
                            </div>
                            <div className="Info-Item">
                                <div className="Label">Certificate Number:</div>
                                <div className="Description">{certificate_number}</div>
                            </div>
                            <div className="Info-Item">
                                <div className="Label">Inspection Date:</div>
                                <div className="Description">
                                    <Moment format="MMM D, YYYY">{certificate.inspection_date}</Moment>
                                </div>
                            </div>
                            <div className="Info-Item">
                                <div className="Label">Valid Until:</div>
                                <div className="Description">
                                    <Moment format="MMM D, YYYY">{certificate.valid_until}</Moment>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="Grid-Item-2">
                        <iframe src={pdf} height="100%" width="100%"/>
                    </div>
                </div>

                <style jsx>{`
                    .Certificate .Banner {
                        background: url(../assets/images/certification-registry-banner.png) left top;
                        background-size: cover;
                        padding: 30px;
                        margin: 30px 0;
                        width: 100%;
                        height: 280px;
                        color: #fff;
                        text-transform: uppercase;
                        font-size: 28px;
                    }

                    .Certificate .Banner h1 {
                        margin: 0;
                    }

                    .Certificate .Banner h2 {
                        margin: 0;
                        font-weight: 300;
                        font-size: 50px;
                    }

                    .Certificate .Grid {
                        margin-bottom: 50px;
                        display: flex;
                        align-items: start;
                        justify-content: start;
                        flex-wrap: wrap;
                    }

                    .Certificate .Grid .Grid-Item-1 {
                        flex-basis: 30%;
                    }

                    .Certificate .Grid .Grid-Item-1 .Info .Info-Item {
                        margin-bottom: 15px;
                    }

                    .Certificate .Grid .Grid-Item-1 .Info .Info-Item .Label {
                        margin-bottom: 5px;
                        font-weight: 700;
                        font-size: 14px;
                    }

                    .Certificate .Grid .Grid-Item-1 .Info .Info-Item .Description {
                        font-size: 16px;
                    }

                    .Certificate .Grid .Grid-Item-2 {
                        flex-basis: 70%;
                    }

                    .Certificate .Grid .Grid-Item-2 iframe {
                        width: 100%;
                        height: 960px;
                    }

                    @media (max-width: 767px) {
                        .Certificate .Banner {
                            font-size: 20px;
                        }

                        .Certificate .Grid {
                            flex-direction: column;
                        }

                        .Certificate .Grid > div {
                            width: 100%;
                            flex-basis: inherit !important;
                        }

                        .Certificate .Grid .Grid-Item-2 iframe {
                            width: 100%;
                            height: 520px;
                        }
                    }
                `}</style>
            </div>
        </div>
    )
}