export default function Error() {
    return (
        <div className="Error">
            <div className="Container">
                <div className="Grid">
                    <div className="Grid-Item-1">
                        <h1>WHOOPS!</h1>
                        <h2>We lost this page</h2>
                        <p>We searched high and low but couldn’t<br/>
                        find what you’re looking for. Let’s find a<br/>
                        better place for you to go.</p>
                        <a href="https://verify.people360.com.ph/">Home</a>
                    </div>
                    <div className="Grid-Item-2">
                        <img src="../assets/images/404.png" />
                    </div>
                </div>
            </div>

            <style jsx>{`

                @font-face {
                    font-family: Roboto-Black;
                    src: url(../assets/fonts/Roboto-Black.ttf);
                }

                @font-face {
                    font-family: Roboto-Light;
                    src: url(../assets/fonts/Roboto-Light.ttf);
                }

                @font-face {
                    font-family: Roboto-Regular;
                    src: url(../assets/fonts/Roboto-Regular.ttf);
                }

                .Error {
                    padding: 60px 0;
                    background-color: #de772a;
                    color: #fff;
                }

                .Error .Grid {
                    margin-bottom: 50px;
                    display: flex;
                    align-items: start;
                    justify-content: start;
                    flex-wrap: wrap;
                }

                .Error .Grid .Grid-Item-1 {
                    flex-basis: 50%;
                }

                .Error .Grid .Grid-Item-1 h1 {
                    font-family: "Roboto-Black", sans-serif;
                    margin: 0;
                    font-size: 48px;
                }

                .Error .Grid .Grid-Item-1 h2 {
                    font-family: "Roboto-Light", sans-serif;
                    margin: 0;
                    font-size: 45px;
                    font-weight: 100;
                    text-transform: uppercase;
                }

                .Error .Grid .Grid-Item-1 p {
                    font-family: "Roboto-Light", sans-serif;
                    margin-top: 30px;
                    margin-bottom: 40px;
                    font-size: 20px;
                    font-weight: 100;
                    line-height: 30px;
                }

                .Error .Grid .Grid-Item-1 a {
                    font-family: "Roboto-Regular", sans-serif;
                    padding: 10px 25px;
                    text-decoration: none;
                    color: #fff;
                    text-transform: uppercase;
                    border: 1px solid #fff;
                    border-radius: 3px;
                }

                .Error .Grid .Grid-Item-2 {
                    flex-basis: 40%;
                }

                .Error .Grid .Grid-Item-2 img {
                    max-width: 100%;
                }

                @media (max-width: 996px) {
                    .Error .Grid {
                        flex-direction: column;
                    }

                    .Error .Grid > div {
                        width: 100%;
                        flex-basis: inherit !important;
                    }

                    .Error .Grid .Grid-Item-1 h1 {
                        font-size: 30px;
                    }
    
                    .Error .Grid .Grid-Item-1 h2 {
                        font-size: 35px;
                    }
    
                    .Error .Grid .Grid-Item-1 p {
                        font-size: 18px;
                    }
                }
            `}</style>
        </div>
    )
}