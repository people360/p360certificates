export default function Footer() {
    return (
        <div className="Footer">
            <div className="Container">
               <div className="Grid">
                    <div className="Grid-Item">
                        <h4>Services</h4>
                        <a href="http://people360.com.ph/services/safety-consultancy">Safety Consultancy </a>
                        <a href="http://people360.com.ph/services/equipment-testing">Construction Heavy<br />Equipment Testing </a>
                        <a href="http://people360.com.ph/services/work-environment-measurement">Work Environment<br />Measurement (WEM) </a>
                        <a href="http://people360.com.ph/services/non-destructive-testing">Non-Destructive<br />Testing (NDT) </a>
                        <a href="http://people360.com.ph/services/trainings">Safety Training Courses</a>
                    </div>
                    <div className="Grid-Item">
                        <h4>Solutions</h4>
                        <a href="http://people360.com.ph/solutions/personal-protective-equipment">Personal Protective<br />Equipment </a>
                        <a href="http://people360.com.ph/solutions/technology-solutions">Technology Solutions</a>
                    </div>
                    <div className="Grid-Item">
                        <h4>Contact Us</h4>
                        <strong>Head Office</strong>
                        <a>Telephone +63.2.83620566/ 83669448 1141 EDSA, Balintawak, Quezon City, 1106 Philippines</a>
                        <strong>Cebu Office</strong>
                        <a>Telephone +63.32.2665769 Carlock Garden Offices, 165 Carlock St., San Nicolas, Cebu City, Cebu 6000 Philippines</a>
                    </div>
                    <div className="Grid-Item">
                        <h4>Follow Us</h4>
                        <div className="Social">
                            <a href="http://www.facebook.com/pages/People360-Consulting-Corporation/154713067950902"><img src="../assets/images/Facebook@2x.png"/></a>
                            <a href="https://www.linkedin.com/company/3050417/"><img src="../assets/images/LinkedIn@2x.png"/></a>
                            <a href="https://twitter.com/#!/People360"><img src="../assets/images/Twitter@2x.png"/></a>
                        </div>
                    </div>
               </div>
               <div className="Copy-Right">© 2011-2020 People360 Consulting Corporation</div>
            </div>

            <style jsx>{`
                .Footer {
                    background-color: #E7E8EC;
                    padding: 25px 15px;
                }

                .Footer .Grid {
                    display: flex;
                    align-items: start;
                    justify-content: start;
                    flex-wrap: wrap;
                }
                
                .Footer .Grid .Grid-Item {
                    padding: 15px;
                    margin-bottom: 20px;
                    flex-basis: 25%;
                }

                .Footer .Grid .Grid-Item h4 {
                    color: #666;
                    font-size: 16px;
                    font-weight: 700;
                    text-transform: uppercase;
                }
                
                .Footer .Grid .Grid-Item strong {
                    color: #666;
                    font-size: 14px;
                }

                .Footer .Grid .Grid-Item a {
                    display: block;
                    margin-bottom: 15px;
                    color: #666;
                    font-size: 14px;
                    text-decoration: none;
                }

                .Footer .Grid .Grid-Item .Social a {
                    display: inline-block;
                    margin-right: 10px;
                }

                .Footer .Grid .Grid-Item .Social a img {
                    width: 25px;
                    height: 25px;
                }

                .Footer .Copy-Right {
                    padding: 15px;
                    color: #666;
                    font-size: 12px;
                }

                @media (max-width: 767px) {
                    .Grid {
                        flex-direction: column;
                    }

                    .Grid > div {
                        width: 100%;
                        flex-basis: inherit !important;
                    }
                }
            `}</style>
        </div>
    )
}