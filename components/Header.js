export default function Header() {
    return (
        <div className="Header">
            <div className="Container">
                <div className="Logo">
                    <a>
                        <img src="../assets/images/Logo.svg" />
                    </a>
                </div>
            </div>

            <style jsx>{`
                .Header {
                    border-bottom: 5px solid #FB6333 !important;
                }

                .Header .Logo {
                    padding: 20px 0;
                    margin: 0;
                }

                .Header .Logo img {
                    width: 100px;
                }
            `}</style>
        </div>
    )
}