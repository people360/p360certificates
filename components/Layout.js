import Head from "next/head";
import Header from "./Header";
import Footer from "./Footer"

import MessengerCustomerChat from 'react-messenger-customer-chat';

export default function Layout(props) {

  return (
    <div className="Layout">
        <Head>
            <title>Certification Registry - People360 Consulting</title>
            <meta charSet="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Head>

        <Header />

        {props.children}

        <Footer />

        <MessengerCustomerChat
          pageId="154713067950902"
          appId="431525141186194"
        />

        <style jsx global>{`
            html,
            body {
                padding: 0;
                margin: 0;
                font-family: Helvetica, sans-serif;
            }

            .Container {
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }

            @media (min-width: 576px) {
                .Container {
                    max-width: 540px;
                }
            }

            @media (min-width: 768px) {
                .Container {
                    max-width: 720px;
                }
            }

            @media (min-width: 992px) {
                .Container {
                    max-width: 960px;
                }
            }

            @media (min-width: 1200px) {
                .Container {
                    max-width: 980px;
                }
            }

            * {
                box-sizing: border-box;
            }
        `}</style>
    </div>
  )
}
