import Layout from "../../components/Layout"
import DataCertificate from "../../components/DataCertificate"
import Error from "../../components/Error"

Certificate.getInitialProps = async (context) => {

  const res = await fetch(`${process.env.STRAPI_API_URL}certificates?id_eq=${context.query.id}&certificate_number_field_2_eq=${context.query.certificate_number}`)
  const json = await res.json()

  if (json.errors) {
    console.error(json.errors)
    throw new Error('Failed to fetch API')
  }

  if( json.length > 0 ) {
    return { data: json[0] }
  } else {
    return { data: false }
  }
}

export default function Certificate (props) {

  if( !props.data || props.data.status == "Draft" || props.data.status == "Processing" ) {
    return (
      <Layout>
        <Error />
      </Layout>
    )
  }

  return (
    <Layout>
      <DataCertificate data={props.data} />
    </Layout>
  )

}