import { useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Table,
  Pagination,
  Spinner,
} from "react-bootstrap";
import Layout from "../components/Layout";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import dayjs from "dayjs";

import CertificateService from "../services/certificate";

export default function Home() {
  const [showTable, setShowTable] = useState(false);
  const [documentType, setDocumentType] = useState();
  const [certificateNumber, setCertificateNumber] = useState();
  const [certificateIssuedTo, setCertificateIssuedTo] = useState();
  const [certificates, setCertificates] = useState([]);
  const [loadingTable, setLoadingTable] = useState(false);

  const searchHandler = async (event) => {
    event.preventDefault();
    event.stopPropagation();
    setLoadingTable(true);
    setShowTable(true);
    const response = await CertificateService.getAllCertificates({
      documentType,
      certificateNumber,
      certificateIssuedTo,
    });
    setCertificates(response);
    setLoadingTable(false);
  };

  return (
    <Layout>
      <div className="Certificates">
        {/* <img className="background-image" src="/assets/images/background.svg" /> */}
        <div className="Container">
          <div className="Grid">
            <div className="Grid-Item-1">
              <h1>VERIFICATION</h1>
              <h2>REGISTRY</h2>
              <p className="subtitle">
                We took one more step to secure your certification’s
                authenticity.
              </p>
              <div className="description-wrapper">
                <p>
                  All our safety inspections and training certificates are now
                  equipped with QR codes. Simply scan and follow the online
                  version of the certificate to verify the document validity and
                  accuracy of content.
                </p>
                <p>
                  Now, you are more assured that your equipments and safety
                  officers are safe and properly trained to meet quality
                  standards that you need for your organization.
                </p>
              </div>
            </div>
          </div>
          <Container>
            <Form onSubmit={searchHandler}>
              <Row className="search-wrapper">
                <Col className="search-wrapper-item" xs={12} sm={12} md={4}>
                  <Form.Label>Document Type</Form.Label>
                  <Form.Control
                    as="select"
                    required
                    onChange={(e) => setDocumentType(e.target.value)}
                  >
                    <option value="" disabled selected>
                      Select...
                    </option>
                    <option value="Certificate of Inspection">
                      Certificate of Inspection
                    </option>
                    <option value="Certificate of Training">
                      Certificate of Training
                    </option>
                  </Form.Control>
                </Col>
                <Col className="search-wrapper-item" xs={12} sm={12} md={4}>
                  <Form.Label>Certificate Issued to</Form.Label>
                  <Form.Control
                    type="text"
                    required
                    placeholder="Enter Name"
                    onChange={(e) => setCertificateIssuedTo(e.target.value)}
                  />
                </Col>
                <Col className="search-wrapper-item" xs={12} sm={12} md={3}>
                  <Form.Label>Certificate Number</Form.Label>
                  <Form.Control
                    type="text"
                    required
                    placeholder="123456789XX"
                    onChange={(e) => setCertificateNumber(e.target.value)}
                  />
                </Col>
                <Col className="search-button-wrapper" sm={12} md={1}>
                  <Button
                    type="submit"
                    variant="primary"
                    className="search-button"
                  >
                    <FontAwesomeIcon icon={faSearch} />
                  </Button>
                </Col>
              </Row>
            </Form>
            {showTable ? (
              <Row className="table-wrapper">
                <Col>
                  <p className="table-header">{documentType}</p>
                  {loadingTable && (
                    <div className="table-loader">
                      <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                      </Spinner>
                    </div>
                  )}
                  {!loadingTable && (
                    <Table hover>
                      <thead>
                        <tr>
                          <th>Certificate No.</th>
                          <th>Name</th>
                          <th>
                            {documentType === "Certificate of Inspection"
                              ? "Inspection Date"
                              : "Course Name"}
                          </th>
                          <th>
                            {documentType === "Certificate of Inspection"
                              ? "Valid Until"
                              : "Course Name"}
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {certificates.map((certificate) => {
                          return (
                            <tr
                              style={{ cursor: "pointer" }}
                              key={certificate.key}
                              onClick={() => {
                                window.open(certificate.url, "_blank");
                              }}
                            >
                              <td>{certificate.certificate_number_field_2}</td>
                              <td>{certificate.company}</td>
                              <td>
                                {documentType === "Certificate of Inspection"
                                  ? dayjs(certificate.inspection_date).format(
                                      "MMM DD, YYYY"
                                    )
                                  : "N/A"}
                              </td>
                              <td>
                                {documentType === "Certificate of Inspection"
                                  ? dayjs(certificate.valid_until).format(
                                      "MMM DD, YYYY"
                                    )
                                  : "N/A"}
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                      {/* <Pagination>
                      <Pagination.First />
                      <Pagination.Prev />
                      <Pagination.Item>1</Pagination.Item>
                      <Pagination.Ellipsis />
                      <Pagination.Item>11</Pagination.Item>
                      <Pagination.Item>12</Pagination.Item>
                      <Pagination.Item>13</Pagination.Item>
                      <Pagination.Ellipsis />
                      <Pagination.Item>21</Pagination.Item>
                      <Pagination.Next />
                      <Pagination.Last />
                    </Pagination> */}
                    </Table>
                  )}
                </Col>
              </Row>
            ) : (
              <>
                <div className="Grid">
                  <div className="Grid-Item-1">
                    <div className="description-wrapper">
                      <p>
                        Starting October 2020, you will be able to see your
                        certificates and stickers with this QR Code feature. To
                        verify the authenticity of a certification holder’s
                        status, simply scan the QR code reflected on the
                        sticker/certificate issued by People360. It should
                        direct you to the online copy of the corresponding
                        certificate.
                      </p>
                      <p>
                        Basta Safety,{" "}
                        <span
                          style={{
                            fontWeight: "bold",
                            color: "#F86333",
                          }}
                        >
                          PEOPLE360!
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </>
            )}
          </Container>
        </div>

        <style jsx>{`
          @font-face {
            font-family: SFProText-Regular;
            src: url(../assets/fonts/SFProText-Regular.ttf);
          }

          @font-face {
            font-family: DIN-Regular;
            src: url(../assets/fonts/DIN-Regular.ttf);
          }
        `}</style>
      </div>
    </Layout>
  );
}
