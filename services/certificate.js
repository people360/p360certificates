import axios from "axios";

import config from "../config";

export default {
  getAllCertificates: async (query) => {
    let queryString = "?";
    if ("documentType" in query && query.documentType) {
      queryString += `document_name=${query.documentType}`;
    }
    if ("certificateNumber" in query && query.certificateNumber) {
      queryString += `&certificate_number_field_2_contains=${query.certificateNumber}`;
    }
    if ("certificateIssuedTo" in query && query.certificateIssuedTo) {
      queryString += `&company_contains=${query.certificateIssuedTo}`
    }
    const { data } = await axios.get(
      `${config.strapi_api}certificates${queryString}`
    );

    return data;
  },
};
